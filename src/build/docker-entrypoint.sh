#!/bin/bash
set -e

stop_cron_daemon() {
    local cron_pid="$1";
    if [ -n "$cron_pid" ] && kill -0 "$cron_pid" &>/dev/null; then
        kill "$cron_pid";
    else
        exit 0
    fi
}

for file in /opt/docker-entrypoint-init.d/*; do
    if [ -f ${file} ];
    then
        case "$file" in
            *.sh)     echo "$0: running $file"; . "$file" ;;
            *)        echo "$0: ignoring $file" ;;
        esac
    fi
    echo
done


for file in /tmp/crontabs/user_level/*.tasks; do
    if [ -f $file ];
    then
        filename=$(basename $file)
        username="${filename%.*}"

        /usr/bin/crontab -u "$username" "$file" > /dev/null
    fi
done

# exec /usr/sbin/cron -f &

# start cron in foreground mode and background it using
# shell semantics so that the PID can be captured
/usr/sbin/cron -f -L15 &
cron_pid=$!

trap "stop_cron_daemon $cron_pid" INT TERM EXIT

wait $cron_pid